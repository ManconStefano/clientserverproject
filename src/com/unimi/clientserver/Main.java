package com.unimi.clientserver;

import java.net.ServerSocket;

public class Main {
    public static int SOCKETPORT = 1212;
    public static void main(String[] args) {
        Server server = new Server();
        server.Connect();
        server.AcceptClient();
    }
}
