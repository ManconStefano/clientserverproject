package com.unimi.clientserver;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

import static com.unimi.clientserver.Main.SOCKETPORT;

public class Server {

    ServerSocket serverSocket = null;
    Socket clientSocket = null;
    BufferedReader in = null;
    PrintWriter out = null;


    void Connect() {
        try {
            serverSocket = new ServerSocket(SOCKETPORT); //created new socket
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("NuovoServerAvviato");
    }


    void AcceptClient() {
        //resto in attesa di una nuova connessione
        try {
            clientSocket = serverSocket.accept();
            System.out.println("New connection from" + clientSocket);

            InputStreamReader inputStreamReader = new InputStreamReader(clientSocket.getInputStream());
            in = new BufferedReader(inputStreamReader);

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(clientSocket.getOutputStream());
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

            while (clientSocket.isConnected()) {
                String string = in.readLine();
                System.out.println("Server dice:" + string);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}

