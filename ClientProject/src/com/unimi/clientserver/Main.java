package com.unimi.clientserver;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;



public class Main {

    public static void main(String[] args) {
        InetAddress serverAddress;
        Socket socket = null;
        BufferedReader in = null, stdIn = null;
        PrintWriter out = null;

        {
            try {
                serverAddress = InetAddress.getLocalHost();
                socket = new Socket(serverAddress, 1212);



                InputStreamReader isr = new InputStreamReader( socket.getInputStream());
                in = new BufferedReader(isr);

                OutputStreamWriter osw = new OutputStreamWriter( socket.getOutputStream());
                BufferedWriter bw = new BufferedWriter(osw);
                out = new PrintWriter(bw, true);

                // creazione stream di input da tastiera
                stdIn = new BufferedReader(new InputStreamReader(System.in));
                String userInput;

                while (true){
                    userInput= stdIn.readLine();
                    out.println(userInput);
                }


            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
